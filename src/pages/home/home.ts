import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  client_name : string;
  client_telephone : string;
  client_adresse : string;
  client_solde : string;
  constructor(public navCtrl: NavController,public http: Http,public global:GlobalProvider) {
    this.getClientById();
  }


  logout(){
    this.global.logout();
   //this.presentConfirm();
  }


  getClientById(){
    this.http.get(this.global.url+"/clients/getClientById.php?id="+this.global.client_id)
    .map(res => res.json())
    .subscribe(res => {
        console.log(res);
        this.client_name = res.client;
        this.client_solde = res.solde_non_affecte;
        if(res.adresse)
          this.client_adresse =  res.adresse;
        else
        this.client_adresse = "";  
        this.client_telephone = res.mobile;
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
    });
  }


}
