import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the ProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {

  client_name : string;
  client_telephone : string;
  client_adresse : string;
  client_solde : string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider) {
    this.getClientById();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilPage');
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }
  getClientById(){
    this.http.get(this.global.url+"/clients/getClientById.php?id="+this.global.client_id)
    .map(res => res.json())
    .subscribe(res => {
        this.client_name = res.client;
        this.client_solde = res.solde_non_affecte;
        this.client_adresse =  res.adresse;
        this.client_telephone = res.mobile;
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
    });
  }

}
