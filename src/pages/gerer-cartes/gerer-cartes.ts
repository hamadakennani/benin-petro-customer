import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { ListeCartesPage } from '../liste-cartes/liste-cartes';

/**
 * Generated class for the GererCartesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gerer-cartes',
  templateUrl: 'gerer-cartes.html',
})
export class GererCartesPage {
  pet: string = "puppies";
  liste_cartes_debit : any[];
  liste_cartes_credit : any[];
  liste_historique : any[];
  carteDebit : string = "";
  carteCredit : string = "";
  client_id : string;
  client_name : string;
  typeOperation : string;
  montant : string = "";
  soldeCarteDebit : string;
  soldeCarteCredit : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,private alertCtrl:AlertController,public loading: LoadingController) {
    this.listeCartesByClient();
    this.client_id = this.global.client_id;
    this.client_name = this.global.client_name;
    //this.liste_cartes_debit = {};
    //this.liste_cartes_debit = {id: this.client_id, libelle: this.client_name};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GererCartesPage');
  }


  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  listeCartesByClient(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+"/clients/listeCartesByClient.php?id="+this.global.client_id)
    .map(res => res.json())
    .subscribe(res => {
      console.log(res);
      console.log("eeeeeeeeeeeeeeeeeeeeeeeeeeeee");
        this.liste_cartes_debit = res;
       // this.liste_cartes_debit.append(res);
        this.liste_cartes_credit = res;
        loading.dismissAll();
        
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
  }


  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Code PIN',
      inputs: [
        {
          name: 'code',
          placeholder: 'Code PIN',
          type:"tel",
          id:"input_code_pin"
        }
      ],
      buttons: [
        {
          text: 'Valider',
          handler: data => {
            
          let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
          loading.present();
            let postData = new FormData();
            postData.append("mobile",this.global.mobile);
            postData.append("password",data.code);
            var hedears = new Headers();
            hedears.append("Content-Type","application/x-www-form-urlencoded");
            hedears.append("Access-Control-Allow-Origin", "*");
            hedears.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            this.http.post(this.global.url+"/checkClientByTelephone.php",postData)
            .map(res => res.json())
            .subscribe(res => {
              if(res == 1){
                this.http.get(this.global.url+"/clients/chargerCarte.php?carteDebit="+this.carteDebit+"&&carteCredit="+this.carteCredit+"&&typeOperation="+this.typeOperation+"&&montant="+this.montant+"&&client_id="+this.client_id)
                .map(res => res.json())
                .subscribe(res => {
                  if(res == 1){
                    this.global.presentAlert('BRAVO',"Opération effectuée avec succès.","success",ListeCartesPage,this);
                    loading.dismissAll();
                  }else{
                    this.global.presentAlert('Erreur',"Impossible cette transaction","erreur",ListeCartesPage,this);
                    loading.dismissAll();
                  }
                    
                }, (err) => {
                  console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                  console.log(err);
                  loading.dismissAll();
                });
                  
              }else{
                  this.SimpleAlert("Erreur","Mot de passe invalide","erreur");
                  loading.dismissAll();
              }
              console.log(res);
              
        
            }, (err) => {
              console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
              console.log(err);
              loading.dismissAll();
            });    
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            
          }
        }
      ]
    });
    alert.present();
  }


  SimpleAlert(title,text,type) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            //this.navCtrl.setRoot(HomePage);
          }
        }
      }]
    });
    alert.present();
  }

  chargerCarte(){
    if(this.carteDebit == this.client_id){
      this.typeOperation = "compteTocarte";
    }
    else if(this.carteCredit == this.client_id){
      this.typeOperation = "carteTocompte";
    }else{
      this.typeOperation = "carteTocarte"
    }
    if(this.carteCredit != "" && this.carteDebit != "" && this.montant != ""){

      this.presentPrompt();

    }else{
      this.global.presentAlert('Erreur',"Merci de remplir tous les champs svp !!!","erreur",ListeCartesPage,this);
    }
    

    console.log(this.typeOperation);
  }

  onChange(){
    if(this.carteDebit != ""){
      var carte_debit = this.liste_cartes_credit.find(c => c.id === this.carteDebit);
      this.soldeCarteDebit = carte_debit.solde;
    }
    if(this.carteCredit != ""){
      var carte_credit = this.liste_cartes_credit.find(c => c.id === this.carteCredit);
      this.soldeCarteCredit = carte_credit.solde;
    }
    
  
    console.log(this.soldeCarteDebit);
    console.log(this.soldeCarteCredit);

  }


  historique(){

    this.http.get(this.global.url+"/clients/getListeHistoriqueByClient.php?id="+this.client_id)
      .map(res => res.json())
      .subscribe(res => {
          this.liste_historique = res;
      }, (err) => {
        console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
        console.log(err);
      });
  }



}
