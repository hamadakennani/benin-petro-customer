import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GererCartesPage } from './gerer-cartes';

@NgModule({
  declarations: [
    GererCartesPage,
  ],
  imports: [
    IonicPageModule.forChild(GererCartesPage),
  ],
})
export class GererCartesPageModule {}
