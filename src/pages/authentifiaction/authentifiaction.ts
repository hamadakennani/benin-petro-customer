import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global';
import 'rxjs/add/operator/map'
import { ChangePasswordPage } from '../change-password/change-password';

@IonicPage()
@Component({
  selector: 'page-authentifiaction',
  templateUrl: 'authentifiaction.html',
})
export class AuthentifiactionPage {
  user : string;
  password : string;
  check : boolean = true;
  clickChangePassword = false;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,public http: Http,public global:GlobalProvider,private storage: Storage,public loading: LoadingController) {
    
      this.storage.get('login').then((val) => {
        //console.log('Your age is', val);
        this.user = val
      });
    
  }

  ionViewDidLoad() {
    
    
  }


  changerPassword(){
    this.navCtrl.push(ChangePasswordPage);
  }

  login(){

    //this.navCtrl.push(ListeCartesPage);
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    let postData = new FormData();
    postData.append("mobile",this.user);
    postData.append("password",this.password);
    var hedears = new Headers();
    hedears.append("Content-Type","application/x-www-form-urlencoded");
    hedears.append("Access-Control-Allow-Origin", "*");
    hedears.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    this.http.post(this.global.url+"/checkClientByTelephone.php",postData)
    .map(res => res.json())
    .subscribe(res => {
      if(res == 1){
          this.check = true;
          this.storage.set('login', this.user);
          this.http.get(this.global.url+'/getCientByTelephone.php?mobile='+this.user+'&&password='+this.password)
          .map(res => res.json())
          .subscribe(res => {
              this.global.client_id = res.id;
              this.global.client_name= res.client;
              this.global.mobile = res.mobile;
              this.navCtrl.setRoot(TabsPage);
              loading.dismissAll();
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            console.log(err);
            loading.dismissAll();
          });
          //this.navCtrl.setRoot(TabsPage);
      }else{
          this.check = false;
          loading.dismissAll();
      }
      console.log(res);
      

    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
    
  }

}
