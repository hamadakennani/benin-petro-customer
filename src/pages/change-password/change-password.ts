import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { AuthentifiactionPage } from '../authentifiaction/authentifiaction';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  mobile:string;
  checkCode: boolean= false;
  code_validation_par_mail : string;
  code_validation : string;
  check:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,public loading: LoadingController,private alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }


  reinitialiser(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/clients/changePassword.php?mobile='+this.mobile+'&&type=codevalidate')
          .map(res => res.json())
          .subscribe(res => {
            if(res == -1){
              this.presentAlert('EREUR',"Ce numéro de téléphone n'existe pas","error");
            }else{
              this.checkCode = true;
              console.log('codeeeeeeeeeeeeeeeeeeeee');
              console.log(res);
              this.code_validation_par_mail = res
            }
            loading.dismissAll();
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            console.log(err);
            loading.dismissAll();
          });
  }

  valider(){
    
    if(this.code_validation == this.code_validation_par_mail){
      let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
      loading.present();
        this.check =false;
        this.http.get(this.global.url+'/clients/changePassword.php?mobile='+this.mobile+'&&type=changepassword')
          .map(res => res.json())
          .subscribe(res => {
            if(res == 1 ){
              loading.dismissAll();
              this.presentAlert('BRAVO',"Votre mot de passe a été changé avec succès","success");
            }
            
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            console.log(err);
            loading.dismissAll();
          });
    }else{
      this.check = true;
    }

  }

  presentAlert(title,text,type) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            this.navCtrl.setRoot(AuthentifiactionPage);
          }

        }
      }],
      enableBackdropDismiss: false
    });
    alert.present();
  }

}
