import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the TransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transaction',
  templateUrl: 'transaction.html',
})
export class TransactionPage {
  id:string;
  carte:string;
  serie :string;
  produit:string;
  montant:string;
  date_transaction :string;
  point_vente :string;
  agent:string;
  quantite:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider) {
    this.id = this.navParams.get("id");
    this.http.get(this.global.url+'/getTransactionById.php?id='+this.id)
      .map(res => res.json())
      .subscribe(res => {
          console.log(res);
          this.carte = res[0].carte;
          this.point_vente = res[0].point_de_vente;
          this.produit = res[0].produit;
          this.montant = res[0].montant;
          this.agent = res[0].agent;
          this.date_transaction = res[0].date_transaction;
          this.quantite = res[0].qte;
          this.serie = res[0].serie;

      }, (err) => {
        console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
        console.log(err);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionPage');
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

}
