import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListeCartesPage } from './liste-cartes';

@NgModule({
  declarations: [
    ListeCartesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListeCartesPage),
  ],
})
export class ListeCartesPageModule {}
