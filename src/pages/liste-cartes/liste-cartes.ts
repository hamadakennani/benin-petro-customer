import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { DetailCartePage } from '../detail-carte/detail-carte';
import { TransactionPage } from '../transaction/transaction';

/**
 * Generated class for the ListeCartesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-liste-cartes',
  templateUrl: 'liste-cartes.html',
})
export class ListeCartesPage {

  pet: string = "puppies";
  client_name : string;
  solde_non_affecte : string;
  solde_affecte : string;
  liste_cartes : any[];
  listeTransaction : any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,public loading: LoadingController) {

    this.getClientById();
    this.listeCartesByClient();
    this.getTransactionByClient();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListeCartesPage');
  }
  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  goToPage(carte_id,qrcodde,libelle){
    this.navCtrl.push(DetailCartePage,{
      "carte_id":carte_id,
      "qrcode":qrcodde,
      "libelle":libelle
    });
  }

  
  detailTransaction(id){
    this.navCtrl.push(TransactionPage,{
      "id":id
    })
  }


  getClientById(){
    this.http.get(this.global.url+"/clients/getClientById.php?id="+this.global.client_id)
    .map(res => res.json())
    .subscribe(res => {
        this.client_name = res.client;
        this.solde_non_affecte = res.solde_non_affecte;
        this.solde_affecte =  res.solde_affecte;
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
    });
  }

  listeCartesByClient(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+"/clients/listeCartesByClient.php?id="+this.global.client_id)
    .map(res => res.json())
    .subscribe(res => {
      console.log(res);
        this.liste_cartes = res;
        loading.dismissAll();
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
  }

  getTransactionByClient(){
    this.http.get(this.global.url+"/clients/getListeTransactionByClient.php?id="+this.global.client_id)
    .map(res => res.json())
    .subscribe(res => {
      console.log(res);
        this.listeTransaction = res;
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
    });
  }

}
