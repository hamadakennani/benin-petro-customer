import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailCartePage } from './detail-carte';

@NgModule({
  declarations: [
    DetailCartePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailCartePage),
  ],
})
export class DetailCartePageModule {}
