import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { TransactionPage } from '../transaction/transaction';
import { HomePage } from '../home/home';

/**
 * Generated class for the DetailCartePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-carte',
  templateUrl: 'detail-carte.html',
})
export class DetailCartePage {
  qrcode : string;
  libelle : string;
  solde : string;
  serie : string;
  carte_id : string;
  listeTransaction : any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,public loading: LoadingController,private alertCtrl:AlertController) {
    
    this.qrcode = this.navParams.get("qrcode");
    this.libelle = this.navParams.get("libelle");
    this.carte_id = this.navParams.get("carte_id");
    this.getInfoParCarte();
    this.ListeTransaction();
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailCartePage');
    console.log(this.navParams.get("libelle"));
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }
  
  goToPage(id){
    this.navCtrl.push(TransactionPage,{
      "id":id
    })
  }


  changeCodePin() {
    let alert = this.alertCtrl.create({
      title: 'Changement de code PIN',
      message: 'voulez-vous vraiment générer un autre code PIN ?',
      buttons: [
        {
          text: 'Confirmer',
          handler: () => {
            console.log('Buy clicked');
            let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
            loading.present();
            this.http.get(this.global.url+'/clients/changeCodePinCarte.php?carte_id='+this.carte_id)
            .map(res => res.json())
            .subscribe(res => {
              if(res == 1){
                this.global.presentAlert('BRAVO',"Le code PIN a été changé avec succès.","success",HomePage,this);
                loading.dismissAll();
              }else{
                this.global.presentAlert('Erreur',"Impossible cette transaction","erreur",DetailCartePage,this);
                loading.dismissAll();
              }
            }, (err) => {
              
              console.log(err);
              loading.dismissAll();
            });
            
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  ListeTransaction(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/clients/getListeTransactionByCarte.php?carte_id='+this.carte_id)
        .map(res => res.json())
        .subscribe(res => {
          //this.global.presentAlert("message",res.client,"success",VentePage,this);
            this.listeTransaction = res;  
            console.log(res);
            loading.dismissAll();
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });

  }

  getInfoParCarte(){
    this.http.get(this.global.url+"/getInfoParCarte.php?qrcode="+this.qrcode)
    .map(res => res.json())
    .subscribe(res => {
        this.solde = res.solde;
        this.serie = res.serie;
        this.carte_id = res.id;
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
    });
  }

}
