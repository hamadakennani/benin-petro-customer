import { Injectable } from '@angular/core';
import { AuthentifiactionPage } from '../../pages/authentifiaction/authentifiaction';
import { App, AlertController } from 'ionic-angular';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class GlobalProvider {
  
  public url : string = 'https://51.38.199.241/api';
  public client_id :string
  public client_name :string
  public mobile:string;
  constructor(private app:App,private alertCtrl:AlertController) {
    console.log('Hello GlobalProvider Provider');
  }



  logout() {
    //this.navCtrl.push(AuthentifiactionPage);
    this.presentConfirm();
    
  } 

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Fermeture de la session',
      message: 'voulez-vous vraiment quitter la session ?',
      buttons: [
        {
          text: 'Confirmer',
          handler: () => {
            console.log('Buy clicked');
            this.app.getRootNav().push(AuthentifiactionPage);
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  presentAlert(title,text,type,page,root) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            root.navCtrl.setRoot(page);
          }
        }
      }]
    });
    alert.present();
  }

}
