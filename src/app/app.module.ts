import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FirstPage } from '../pages/first/first';
import { AuthentifiactionPage } from '../pages/authentifiaction/authentifiaction';
import { GlobalProvider } from '../providers/global/global';
import { ProfilPage } from '../pages/profil/profil';
import { ListeCartesPage } from '../pages/liste-cartes/liste-cartes';
import { DetailCartePage } from '../pages/detail-carte/detail-carte';
import { TransactionPage } from '../pages/transaction/transaction';
import { IonicStorageModule } from '@ionic/storage';
import { GererCartesPage } from '../pages/gerer-cartes/gerer-cartes';
import { ChangePasswordPage } from '../pages/change-password/change-password';





@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    FirstPage,AuthentifiactionPage,ProfilPage,
    ListeCartesPage,DetailCartePage,TransactionPage,GererCartesPage,ChangePasswordPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,HttpModule,
    IonicModule.forRoot(MyApp),IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AuthentifiactionPage,FirstPage,ProfilPage,ListeCartesPage,
    DetailCartePage,TransactionPage,GererCartesPage,ChangePasswordPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider
  ]
})
export class AppModule {}
