import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Platform,IonicPage, App } from 'ionic-angular';

import { FirstPage } from '../pages/first/first';
import { ListeCartesPage } from '../pages/liste-cartes/liste-cartes';
import { GererCartesPage } from '../pages/gerer-cartes/gerer-cartes';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = FirstPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public app: App) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  goTopage(){
    //this.nav.push(ListeCartesPage);
    this.app.getActiveNav().setRoot(ListeCartesPage); 
  }

  goTopagee(){
    //this.nav.push(ListeCartesPage);
    this.app.getActiveNav().setRoot(GererCartesPage); 
  }
}
